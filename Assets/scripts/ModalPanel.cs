﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ModalPanel : MonoBehaviour {

	public Text fact;
	public Image iconImage;
	public Button continueButton;
	public GameObject modalPanelObject;

	public static ModalPanel modalPanel;

	public static ModalPanel Instance() {
		if (!modalPanel) {
			modalPanel = FindObjectOfType (typeof(ModalPanel)) as ModalPanel;
			if (!modalPanel) {
				Debug.LogError ("There needs to be one active ModalPanel script on a GameObject in your scene!");
			}
		}

		return modalPanel;
	}

	public void Choice(string fact)
	{
		OpenPanel ();
		//we want to add our own listeners...
		continueButton.onClick.RemoveAllListeners ();
//		continueButton.onClick.AddListener (continuePlaying);
		continueButton.onClick.AddListener(ClosePanel);

		this.fact.text = fact;
		this.iconImage.gameObject.SetActive (true);
		continueButton.gameObject.SetActive (true);

	}

	void ClosePanel() {
		modalPanelObject.SetActive (false);
	}

	void OpenPanel() {
		modalPanelObject.SetActive (true);
	}
}

