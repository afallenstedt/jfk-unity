﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

	Deck deck;
	ComputerAI compAi;

	public delegate void CardDiscardedEventHandler(Card card);
	public event CardDiscardedEventHandler CardDiscarded;

	public void OnPointerEnter(PointerEventData eventData) {
		if(eventData.pointerDrag == null)
			return;
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null) {
			d.placeholderParent = this.transform;
		}
	}
	
	public void OnPointerExit(PointerEventData eventData) {

		if(eventData.pointerDrag == null)
			return;

		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null && d.placeholderParent==this.transform) {
			d.placeholderParent = d.parentToReturnTo;
		}
	}
	
	public void OnDrop(PointerEventData eventData) {
		Debug.Log (eventData.pointerDrag.name + " was dropped on " + gameObject.name);

		// Define what is being dragged by the mouse
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();

		//define what table we dropped stuff on
		string tag = this.transform.tag;

		//if the card is not null and it is not dropped on the same hand...(like I pick up a card to reorganize it...)
		if(d != null && d.parentToReturnTo != this.transform) {

			//what table was the card dropped on?
			switch (tag)
			{
			case "Table":								
				//execute this cards OnDrop();
				eventData.pointerDrag.GetComponent<IDraggable> ().OnDrop ();

				break;

			case "Discard":
				if (CardDiscarded != null) {
					CardDiscarded (eventData.pointerDrag.gameObject.GetComponent<Card>());
				}
				break;
			default:

				Debug.Log ("You can't give your card to the other player dummy.");
				break;
			} 
		}
	}
}
