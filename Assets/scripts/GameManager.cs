﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameObject deck;
	public GameObject discardDropZone;

	public GameObject Computer;
	private ComputerAI aiComponent;
	private Deck deckComponent;

	[SerializeField]
	private Text playerScore;

	[SerializeField]
	private Text computerScore;

	[SerializeField]
	private Text gameStateText;

	[SerializeField]
	private Text playerInhibitorsText;

	[SerializeField]
	private Text computerInhibitorsText;

	private static int pointsToWin = 900;

	private GameObject playersHand;

	public static GameObject computersHand;

	//create modal panel
	private ModalPanel modalPanel;

	//inhibitors text for UI
	public static string playerInhibitors = "None";

	public static string computerInhibitors = "None";

	//inhibitor bools for game state check
	public static bool playerInhibited = false;

	public static bool computerInhibited = false;

	//whose turn
	public static bool playerTurn = true;

	public static bool gameStarted = false;

	//player score
	public static int score;

	//computer score
	public static int compScore;

	public void MoveCardToTable(Transform aCard, string whichDropZone)
	{
		//what table are we dropping on? 
		GameObject table = GameObject.FindWithTag (whichDropZone);
		//move that card to the table
		aCard.transform.SetParent (table.transform);
		if (whichDropZone == "Table") {
			//if it's on the table, then play this cards' OnDrop();
			onCardDropped(aCard.gameObject.GetComponent<Card>());
		} else {
			//otherwise this is a very poopy card and should be discarded. 
			Debug.Log ("DISCARDING");
			AcceptDrop(aCard.gameObject.GetComponent<Card>());

//			deck.DealOneCard ();
		}
	}
		
	public void onCardDropped(Card card) {
		switch(card.type) {
		case "points":
			onPointsDropped (card);
			break;
		case "media":
		case "espionage":
		case "military":
		case "technology":
			if (card.attack) {
				AttackAndInhibit ((AttackCard) card);
			} else {
				CounterAndNeutralize((CounterCard) card);
			}
			break;
		}
	}

	public void onPointsDropped(Card card) {

		//whose turn is it
		switch (GameManager.playerTurn) {
		//players turn
		case true:
			//if the player is not inhibited then
			if (!GameManager.playerInhibited) {
				//then add some points bro
				AddPoints ((PointsCard) card);
				AcceptDrop ((PointsCard)card);

				//player is inhibited...
			} else {
				//don't add those points
				//put the card back
				Draggable d = card.gameObject.GetComponent<Draggable>();
				card.transform.SetParent (d.parentToReturnTo); 
			}
			break;
		case false:
			//if the computer is not inhibited then
			if (!GameManager.computerInhibited) {
				//then add some points bro
				AddPoints ((PointsCard) card);
				AcceptDrop ((PointsCard)card);

				//computer is inhibited...
			} else {
				//don't add those points
				//put the card back
				Draggable d = card.gameObject.GetComponent<Draggable>();
				card.transform.SetParent (d.parentToReturnTo); 
			}
			break;
		}
}

	void AcceptDrop(Card card) {
		Destroy (card.gameObject);
		Destroy (card.GetComponent<Draggable> ().placeholder);
		GameManager.ChangeTurn ();
		deckComponent.DealOneCard ();
		//if it's the computers turn, let the computer play their card
		if (!GameManager.playerTurn) {
			aiComponent.ChooseCard ();
		}
	}

	void AddPoints(PointsCard card) {
		//whose turn is it...?
		switch (GameManager.playerTurn) {
		//players turn
		case true:
				GameManager.score += card.getPoints();
			break;
			//computers turn
		case false:
				GameManager.compScore += card.getPoints();
			break;
		}
			Debug.Log ("adding points " + card.getPoints());
	}

	void AttackAndInhibit (AttackCard card)
	{
		//whose turn is it?
		switch(playerTurn)
		{
		//players turn
		case true:
			//player has just attempted to play an attack card on computer
			//if the computer is not already inhibited...
			if (!computerInhibited) {
				Debug.Log ("inhibit with " + card.type);	
				//make it inhibitied
				computerInhibited = true;
				Debug.Log ("Is Comp inhibited? " + computerInhibited);

				//define what the inhibitor is and update the gamemanager
				computerInhibitors = card.type;
				//activate OnDrop to change turn and deal a card. 
				AcceptDrop(card);

				//the player has attempted to attack the computer who is already inhibited.
				//Or the player is inhibited and has attempted to play a not valid card. Not valid. Sad.
			} else {
				//put the card back and do not activate the cards
				Draggable d = this.GetComponent<Draggable>();
				this.transform.SetParent (d.parentToReturnTo);
			}
			break;
			//computers turn
		case false:
			//computer has just attempted to play an attack card on player
			//if the player is not already inhibited...
			if (!playerInhibited) {
				Debug.Log ("inhibit with " + card.type);	
				//make it inhibitied
				playerInhibited = true;
				Debug.Log ("Is player inhibited? " + playerInhibited);

				//define what the inhibitor is and update the gamemanager
				playerInhibitors = card.type;
				//activate OnDrop to change turn and deal a card. 
				AcceptDrop(card);

				//the computer has attempted to attack the player who is already inhibited.
				//Or the computer is inhibited and has attempted to play a not valid card. Not valid. Sad.
			} else {
				//put the card back and do not activate the cards
				Draggable d = this.GetComponent<Draggable>();
				this.transform.SetParent (d.parentToReturnTo);
			}
			break;
		}
	}

	void CounterAndNeutralize(CounterCard card)
	{
		//whose turn is it?
		switch(playerTurn)
		{
		//player turn
		case true:
			//player has attempted to play a counter card to remove their inhibitor. 
			//card should only be played if they are inhibited, and its of the correct type. 
			//if the player is inhibited and their inhibitors looks like the card type. 
			if (playerInhibited && playerInhibitors == card.type) {
				//then uninhibit the player
				playerInhibited = false;
				playerInhibitors = "none";

				//accept the card
				AcceptDrop(card);
			} else {
				//put the card back and do not activate the cards. Player did not play a valid card.
				Draggable d = card.GetComponent<Draggable>();
				card.transform.SetParent (d.parentToReturnTo);
			}
			break;

			//computer turn
		case false:
			//computer has attempted to play a counter card to remove their inhibitor. 
			//card should only be played if they are inhibited, and its of the correct type. 
			//if the computer is inhibited and their inhibitors looks like the card type. 
			if (computerInhibited && computerInhibitors == card.type) {
				//then uninhibit the player
				computerInhibited = false;
				computerInhibitors = "none";

				//accept the card
				AcceptDrop(card);
			} else {
				//put the card back and do not activate the cards. Player did not play a valid card.
				Draggable d = card.GetComponent<Draggable>();
				card.transform.SetParent (d.parentToReturnTo);
			}
			break;
		}
	}

	void Start () {
		//modify UI with some instructions
		gameStateText.text = "Enter the card amounts you want and then hit spacebar to play!";

		//define the computers hands and the players hands
		playersHand = GameObject.FindWithTag ("Player");
		computersHand = GameObject.FindWithTag ("Computer");

		//you can build a custom deck before the game starts.
		ListenForCardAmountValues ();

		deckComponent = deck.GetComponent<Deck> ();
		aiComponent = Computer.GetComponent<ComputerAI> ();

		deckComponent.droppedDel = onCardDropped;

		//when a card is discarded 
		discardDropZone.GetComponent<DropZone> ().CardDiscarded += AcceptDrop;
	}

	
	// Update is called once per frame
	void Update () {

		//if the game has not started and player presses space bar.
		if (!gameStarted && Input.GetKeyUp(KeyCode.Space)) {
			StartGame ();
		}

		//if the game has started
		if (gameStarted) {
			UpdateScoreBoard ();
			CheckCardCount ();
			DisableOpponentsHand ();
		}

		//if the game has started and the score is greater than or equal to winning Points
		if (gameStarted && score >= pointsToWin || compScore >= pointsToWin){
			EndGame ();
		}
	}

	void Awake()
	{
		//create an instance of a modal panel.
		modalPanel = ModalPanel.Instance ();
	}

	void ListenForCardAmountValues()
	{
		//listen for input fields so we can modify card counts when game begins
		//find the game objects
		GameObject points25InputGo = GameObject.FindGameObjectWithTag("Points 25 Input");
		GameObject points50InputGo = GameObject.FindGameObjectWithTag("Points 50 Input");
		GameObject points75InputGo = GameObject.FindGameObjectWithTag("Points 75 Input");
		GameObject points100InputGo = GameObject.FindGameObjectWithTag("Points 100 Input");
		GameObject points200InputGo = GameObject.FindGameObjectWithTag("Points 200 Input");
		GameObject attackInputGo = GameObject.FindGameObjectWithTag("Attack Input");
		GameObject counterInputGo = GameObject.FindGameObjectWithTag("Counter Input");

		//get the input field from them
		InputField points25Input = points25InputGo.GetComponent<InputField>();
		InputField points50Input = points50InputGo.GetComponent<InputField>();
		InputField points75Input = points75InputGo.GetComponent<InputField>();
		InputField points100Input = points100InputGo.GetComponent<InputField>();
		InputField points200Input = points200InputGo.GetComponent<InputField>();
		InputField attackInput = attackInputGo.GetComponent<InputField>();
		InputField counterInput = counterInputGo.GetComponent<InputField>();

		//add event listener
		points25Input.onValueChanged.AddListener (Deck.Set25Points);
		points50Input.onValueChanged.AddListener (Deck.Set50Points);
		points75Input.onValueChanged.AddListener (Deck.Set75Points);
		points100Input.onValueChanged.AddListener (Deck.Set100Points);
		points200Input.onValueChanged.AddListener (Deck.Set200Points);
		attackInput.onValueChanged.AddListener (Deck.SetAttack);
		counterInput.onValueChanged.AddListener (Deck.SetCounter);
	}

	void UpdateScoreBoard()
	{
		//update point values UI so we know how many points each person has on UI.
		playerScore.text = "Player: " + score + " Points";
		computerScore.text = "Computer: " + compScore + " Points";

		//update the gamestate UI so we know whose turn it is.
		gameStateText.text = (playerTurn) ? "Player's Turn" : "Master Computer's Turn";

		//update the inhibitors UI so we know who is inhibited 
		//and whether or not they can play
		playerInhibitorsText.text = "Player Inhibitors: " + playerInhibitors;
		computerInhibitorsText.text = "Computer Inhibitors: " + computerInhibitors;

		//if a player is inhibited, change their InhibitorsText to red
		playerInhibitorsText.color = (playerInhibited) ? Color.red : Color.white;
		computerInhibitorsText.color = (computerInhibited) ? Color.red : Color.white;

	}

	void StartGame()
	{
		//establish initial values
		score = 0;
		compScore = 0;
		gameStarted = true;

		if (!playerInhibited) {
			playerInhibitorsText.text = "Player Inhibitors: None";
		}

		if (!computerInhibited) {
			computerInhibitorsText.text = "Computer Inhibitors: None";
		}


		deckComponent.LoadCards ();
		//Shuffle Deck
		deckComponent.DeckOfCards = Deck.Fisher_Yates_CardDeck_Shuffle (deckComponent.DeckOfCards);
	
		//deal them cards
		deckComponent.DealCards(playersHand);
		deckComponent.DealCards(computersHand);

		//deal the extra card to the player whose turn it is...
		deckComponent.DealOneCard ();

		//create a test modal panel
		modalPanel.Choice ("\nGreetings human. Are you ready to play a card game? Drag your cards from the bottom row onto the green row. You can add points with the points card; disable your opponent with a red card, and enable yourself with a matching blue card. If you can't do anything, you can put your card in a discard pile. The first person to 900 points wins.   ");
		Debug.Log (modalPanel);

	}

	void EndGame()
	{
		gameStateText.text = (score >= pointsToWin) ? "Hooray! Player won!" : "Master Computer won!";

		gameStarted = false;

	}

	public static void ChangeTurn()
	{
		playerTurn = !playerTurn;

	}

	void DisableOpponentsHand()
	{
		//obtain all the children
		int compChildren = computersHand.transform.childCount;

		for (int i = 0; i < compChildren; i++) {
			//set blocksRaycasts to false
			computersHand.transform.GetChild (i).GetComponent<CanvasGroup> ().blocksRaycasts = false;
		}
	}

	void CheckCardCount()
	{
		//when we are very low on cards, we will add a new deck to the game.
		if (deckComponent.DeckOfCards.Count < 2) {
			deckComponent.LoadCards ();
			deckComponent.DeckOfCards = Deck.Fisher_Yates_CardDeck_Shuffle (deckComponent.DeckOfCards);
		}
	}
}
