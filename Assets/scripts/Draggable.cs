﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	//the parent UI element to return to if card is dragged in bad area.
	public Transform parentToReturnTo = null;

	//the placeholder gameobject's parent so cards move aside before a player drops their card. 
	public Transform placeholderParent = null;
	public GameObject placeholder = null;

	public void OnBeginDrag(PointerEventData eventData) {
		Debug.Log ("OnBeginDrag");

		//create placeholder
		CreatePlaceHolderCard();

		SetParentOfCardAndPlaceHolder();

		//so we can see which element we will be dropping the card on. 
		GetComponent<CanvasGroup>().blocksRaycasts = false;

	}
	
	public void OnDrag(PointerEventData eventData) {
		//Debug.Log ("OnDrag");

		//Move the card we picked up to the pointer's position
		this.transform.position = eventData.position;

		//Switch Placeholder parent if necesssary
		ShouldPlaceholderParentChange();
			
		//Position Placeholder between cards
		PositionPlaceholder();

	}
	
	public void OnEndDrag(PointerEventData eventData) {
			
		PositionCardAndRemovePlaceholder ();
	}

	void CreatePlaceHolderCard()
	{

		//create the gameobject
		placeholder = new GameObject();
		placeholder.transform.SetParent( this.transform.parent );
		LayoutElement le = placeholder.AddComponent<LayoutElement>();
		le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
		le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
		le.flexibleWidth = 0;
		le.flexibleHeight = 0;

		//set the gameobjects parent
		placeholder.transform.SetSiblingIndex( this.transform.GetSiblingIndex() );
	}

	void SetParentOfCardAndPlaceHolder()
	{
		//set the parent to return to the placeholder's immediate parent
		parentToReturnTo = this.transform.parent;

		//save this parent
		placeholderParent = parentToReturnTo;

		//on whatever is being dragged, move it out of the hand so it can "float" in gamespace
		this.transform.SetParent( this.transform.parent.parent );
	}

	void PositionPlaceholder()
	{
		//Position placeholder between cards if necessasary. 
		int newSiblingIndex = placeholderParent.childCount;
		for(int i=0; i < placeholderParent.childCount; i++) {
			if(this.transform.position.x < placeholderParent.GetChild(i).position.x) {

				newSiblingIndex = i;

				if(placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;

				break;
			}
		}

		placeholder.transform.SetSiblingIndex(newSiblingIndex);
	}

	void ShouldPlaceholderParentChange()
	{
		//If the card moves to new valid area, switch the placeholder's parent
		if (placeholder.transform.parent != placeholderParent) {
			Debug.Log ("switch the placeholder parent");
			placeholder.transform.SetParent(placeholderParent);
		}
	}

	 void  PositionCardAndRemovePlaceholder()
	{
		this.transform.SetParent( parentToReturnTo );
		this.transform.SetSiblingIndex( placeholder.transform.GetSiblingIndex() );
		GetComponent<CanvasGroup>().blocksRaycasts = true;

		Destroy(placeholder);
	}
}
