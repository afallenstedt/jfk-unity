﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCard : Card {
	
	// Use this for initialization
	public override void Start () {

		base.Start ();
	}

	public override void OnDrop() {

		Debug.Log ("attacking with " + type);
		if (onDropped != null) {
			onDropped (this);
		}
	}


}
