﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour, IDraggable {


	public delegate void OnCardDropped(Card card);
	public OnCardDropped onDropped;

	ComputerAI compAi;
	public GameObject deck;
	Deck deckComponent;



	//type of card can be "media", "technology", "military", "points", or "espionage". CaSe seNsiTivE
	[SerializeField]
	public string type;

	[SerializeField]
	public bool attack;

	[SerializeField]
	public bool counter;

	public virtual void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}
		

	// implemented from IDraggable
	public virtual void OnDrop() {

		if (onDropped != null) {
			onDropped (this);
		}

	}
}
