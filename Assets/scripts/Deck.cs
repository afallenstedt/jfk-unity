﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour {

	public Card.OnCardDropped droppedDel;

	public static GameObject espionageAttack;

	public static GameObject espionageCounter;

	public static GameObject militaryAttack;

	public static GameObject militaryCounter;

	public static GameObject mediaAttack;

	public static GameObject mediaCounter;

	public static GameObject technologyAttack;

	public static GameObject technologyCounter;

	public static GameObject points25;

	public static GameObject points50;

	public static GameObject points75;

	public static GameObject points100;

	public static GameObject points200;


	//amount of attack cards per type of card
	public static int attackAmount = 3;

	//amount of counter cards per type of card
	public static int counterAmount = 7;

	//amount of cars with value of 25 points
	public static int points25Amount= 10;

	//amount of cars with value of 50 points
	public static int points50Amount= 10;

	//amount of cars with value of 75 points
	public static int points75Amount= 10;

	//amount of cars with value of 100 points
	public static int points100Amount= 10;

	//amount of cars with value of 200 points
	public static int points200Amount= 2;

	[SerializeField]
	public List<GameObject> DeckOfCards = new List<GameObject>();

	public static GameObject playersHand;

	public void DealCards(GameObject hand)
	{
		Card cardComponent;
		List<int> toRemove = new List<int>();
		int pointCardsDealt = 0;
		int countersDealt = 0;
		int attacksDealt = 0;

		for (int i = 0; i < DeckOfCards.Count; i++) {
			cardComponent = DeckOfCards [i].GetComponent<Card> ();
			if (attacksDealt < 1 && cardComponent.attack) {
				CopyCardToHand (hand, DeckOfCards [i]);
				attacksDealt++;
				toRemove.Add (i);
				continue;
			}
			if (countersDealt < 1 && cardComponent.counter) {
				CopyCardToHand (hand, DeckOfCards [i]);
				countersDealt++;
				toRemove.Add (i);
				continue;
			}
			if (pointCardsDealt < 3 && cardComponent.type == "points") {
				CopyCardToHand(hand, DeckOfCards[i]);	
				pointCardsDealt++;
				toRemove.Add (i);
				continue;
			}

		}

		//remove the cards that were copied from the list
		for (int j = toRemove.Count - 1; j >= 0; j -= 1) {
			DeckOfCards.RemoveAt (toRemove [j]);
		}
	}

	void CopyCardToHand(GameObject hand, GameObject card)
	{
		card.transform.SetParent(hand.transform);
		card.GetComponent<Card> ().onDropped = droppedDel;
	}

	public void DealOneCard()
	{
		GameObject playersHand = GameObject.FindWithTag ("Player");
		GameObject computersHand = GameObject.FindWithTag ("Computer");
		Transform cardParent;

		//define parent
		if (GameManager.playerTurn == true) {
			cardParent = playersHand.transform;
		} else {
			cardParent = computersHand.transform;
		}

		//define next card
		GameObject nextCard = DeckOfCards [0];
		//assign delegate function to that card
		nextCard.GetComponent<Card> ().onDropped = droppedDel;
		//move card to desired hand
		nextCard.transform.SetParent (cardParent);
		//remove card from deck
		DeckOfCards.RemoveAt (0);
	}

	public static List<GameObject> Fisher_Yates_CardDeck_Shuffle (List<GameObject>aList) 
	{

		System.Random _random = new System.Random ();

		GameObject myGO;

		int n = aList.Count;
		for (int i = 0; i < n; i++)
		{
			// NextDouble returns a random number between 0 and 1.
			// ... It is equivalent to Math.random() in JS.
			int r = i + (int)(_random.NextDouble() * (n - i));
			myGO = aList[r];
			aList[r] = aList[i];
			aList[i] = myGO;
		}

		return aList;
	}

	public void AddToDeckOfCards(GameObject card, int amount)
	{
		for (int i = 0; i < amount; i++) {
			DeckOfCards.Add (Instantiate (card, gameObject.transform));
		}
	}

	public void LoadCards() {
		
		espionageAttack = (Resources.Load<GameObject> ("prefabs/EspionageAttackCard"));
		espionageCounter = (Resources.Load<GameObject> ("prefabs/EspionageCounterCard"));

		militaryAttack = (Resources.Load<GameObject> ("prefabs/MilitaryAttackCard"));
		militaryCounter = (Resources.Load<GameObject> ("prefabs/MilitaryCounterCard"));

		technologyAttack = (Resources.Load<GameObject> ("prefabs/TechnologyAttackCard"));
		technologyCounter = (Resources.Load<GameObject> ("prefabs/TechnologyCounterCard"));

		mediaAttack = (Resources.Load<GameObject> ("prefabs/MediaAttackCard"));
		mediaCounter = (Resources.Load<GameObject> ("prefabs/MediaCounterCard"));

		points25 = (Resources.Load<GameObject> ("prefabs/25PointsCard"));
		points50 = (Resources.Load<GameObject> ("prefabs/50PointsCard"));
		points75 = (Resources.Load<GameObject> ("prefabs/75PointsCard"));
		points100 = (Resources.Load<GameObject> ("prefabs/100PointsCard"));
		points200 = (Resources.Load<GameObject> ("prefabs/200PointsCard"));

		//create Attack cards
		AddToDeckOfCards(espionageAttack, attackAmount);
		AddToDeckOfCards(militaryAttack, attackAmount);
		AddToDeckOfCards(technologyAttack, attackAmount);
		AddToDeckOfCards(mediaAttack, attackAmount);

		//create counter cards
		AddToDeckOfCards(espionageCounter, counterAmount);
		AddToDeckOfCards(militaryCounter, counterAmount);
		AddToDeckOfCards(technologyCounter, counterAmount);
		AddToDeckOfCards(mediaCounter, counterAmount);

		//create points cards
		AddToDeckOfCards(points25, points25Amount);
		AddToDeckOfCards(points50, points50Amount);
		AddToDeckOfCards(points75, points75Amount);
		AddToDeckOfCards(points100, points100Amount);
		AddToDeckOfCards(points200, points200Amount);


	
	}


	public static void Set25Points(string amount) {
		points25Amount = int.Parse(amount);	
	}
	public static void Set50Points(string amount) {
		points50Amount = int.Parse(amount);	
	}
	public static void Set75Points(string amount) {
		points75Amount = int.Parse(amount);	
	}
	public static void Set100Points(string amount) {
		points100Amount = int.Parse(amount);	
	}
	public static void Set200Points(string amount) {
		points200Amount = int.Parse(amount);	
	}
	public static void SetCounter(string amount){
		counterAmount = int.Parse(amount);
	}
	public static void SetAttack(string amount){
		attackAmount = int.Parse(amount);
	}
}
