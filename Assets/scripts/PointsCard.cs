﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsCard : Card {

	[SerializeField]
	private int points;

	// Use this for initialization
	public override void Start () {
		base.Start ();
	}

	public int getPoints() {
		return points;
	}
	
	public override void OnDrop() {

		if (onDropped != null) {
			onDropped (this);
		}
	}
}
