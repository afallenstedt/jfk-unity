﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerAI : MonoBehaviour {

	Deck deck;

	public static GameObject computersHand;

	public GameObject gameManager;

	private GameManager myManager;

	void Start()
	{
		computersHand = GameObject.FindWithTag("Computer");
		myManager = gameManager.GetComponent<GameManager> ();
	}

	public void ChooseCard()
	{
		//		this is the magical function that lets the computer play. It looks at the first card and asks if it can play it. 

		for (int i = 0; i < computersHand.transform.childCount; i++) {
			Transform aCard = computersHand.transform.GetChild (i);
			bool aCardAttack = aCard.GetComponent<Card> ().attack;
			bool aCardCounter = aCard.GetComponent<Card> ().counter;
			string aCardType = aCard.GetComponent<Card> ().type;
			Debug.Log(aCardType);

			//Did I go through all of my cards? then discard one...
			if (i == computersHand.transform.childCount -1) {
				//rather than destroying the last card, lets destroy the first card. 
				myManager.MoveCardToTable(computersHand.transform.GetChild(1), "Discard");
				break;
				//if I am NOT inhibited
			} else if (!GameManager.computerInhibited) {
				//if the player is NOT inhibited and I have an attack card
				if (!GameManager.playerInhibited && aCardAttack) {
					//then play that card on the player
					myManager.MoveCardToTable (aCard, "Table");
					break;
					//else if I don't have an attack card, and I have a points card
				} else if (aCardType == "points") {
					//then play the points card	
					myManager.MoveCardToTable (aCard, "Table");
					break;
				} else {
					continue;
				}
				//else I AM inhibited
			} else {
				//if I have a counter card that matches the type I am inhibited
				if (aCardCounter && aCardType == GameManager.computerInhibitors) {
					//then play that card
					myManager.MoveCardToTable (aCard, "Table");
					break;
					//else if the player is not inhibitied and I have an attack card
				} else if (!GameManager.playerInhibited && aCardAttack) {
					//then play that card
					myManager.MoveCardToTable (aCard, "Table");
					break;
				} else {
					//else I can't play a card on the table
					continue;
				}
			} 
		}
	}
		
}


