# Cold War Card Game - OHS 


Currently, this is a prototype repo for the JFK cold war card game for Oregon Historical Society. It was built in Unity and is currently in "scratching our head" phase.

## Installation

* This repository is using [git LFS](https://git-lfs.github.com/), a tool that performs large file versioning.
* You will need [Unity 5.0 +](https://unity3d.com/) to work in development.

## Usage

TODO: Write usage instructions

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -m 'Add some feature'`
4. Pull the **dev** branch: `git pull origin dev`
5. Merge your feature branch with dev to detect & resolve conflicts: `git merge my-new-feature`
6. If all is well, push to the dev branch.  

## License

TODO: Write license
